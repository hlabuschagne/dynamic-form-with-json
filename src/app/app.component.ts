import { QuestionBase } from './FormBuilder/QuestionBase';
import { environment } from './../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { ReadJsonService } from './FormBuilder/read-json.service';
import {
  Component,
  ComponentFactoryResolver,
  Input,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
} from '@angular/forms';
import { FormGeneratorService } from './FormBuilder/form-generator.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [FormGeneratorService, ReadJsonService]
})
export class AppComponent {
  title = 'Generating Dynamic form with json';
  @ViewChild('container', { read: ViewContainerRef }) container!: ViewContainerRef;
  @Input() newBlankForm: FormGroup;
  languageSelectedUser: string = '';
  payload: any;
  questionsVar: any;
  languageLabelSelected: string[];
  languageLabelVar!: any;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private fb: FormBuilder,
    private readJsonService: ReadJsonService,
    private httpClient: HttpClient
  ) {
    this.newBlankForm = this.fb.group({});
  }

  //Generate form with read service
  generateFormFromJson() {
    const jsonFileDirectory = '../assets/questionsFile.json';
    this.languageSelectedUser = (<HTMLInputElement>(document.getElementById('languageselect'))).value;
    this.readJsonService.getJsonInputFields(this.languageSelectedUser, this.fb, this.newBlankForm, this.container, this.componentFactoryResolver, jsonFileDirectory)
  }

  // Submit function
  	clickFunction() {
		this.payload = JSON.stringify(this.newBlankForm.getRawValue());

		this.httpClient.post('http://localhost:8080/datastringarray', JSON.stringify(this.newBlankForm.getRawValue()))
					.subscribe();
	}
}
