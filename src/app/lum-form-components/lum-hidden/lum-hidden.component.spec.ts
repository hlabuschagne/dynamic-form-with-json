import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LumHiddenComponent } from './lum-hidden.component';

describe('LumHiddenComponent', () => {
  let component: LumHiddenComponent;
  let fixture: ComponentFixture<LumHiddenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LumHiddenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LumHiddenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
