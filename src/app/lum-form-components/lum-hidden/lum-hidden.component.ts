import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lum-hidden',
  templateUrl: './lum-hidden.component.html',
  styleUrls: ['./lum-hidden.component.css']
})
export class LumHiddenComponent implements OnInit {
  id: string;
  languageId: string;
  value: any;
  formFromClass: FormGroup;

  constructor() { }

  ngOnInit(): void {
  }

}
