import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LumTimeComponent } from './lum-time.component';

describe('LumTimeComponent', () => {
  let component: LumTimeComponent;
  let fixture: ComponentFixture<LumTimeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LumTimeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LumTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
