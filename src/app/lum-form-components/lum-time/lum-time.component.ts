import { FormGroup } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-lum-time',
  templateUrl: './lum-time.component.html',
  styleUrls: ['./lum-time.component.css']
})
export class LumTimeComponent implements OnInit {
  label: string;
  id: string;
  languageId: string;
  @Input() value: any;
  formFromClass:FormGroup;
  dataTable: any;

  constructor() { }

  ngOnInit(): void {
  }

}
