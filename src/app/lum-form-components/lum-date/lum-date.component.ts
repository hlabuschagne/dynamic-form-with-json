import { FormGroup } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-lum-date',
  templateUrl: './lum-date.component.html',
  styleUrls: ['./lum-date.component.css']
})
export class LumDateComponent implements OnInit {
  label: string;
  readOnlyProp: boolean;
  id: string;
  languageId: string;
  @Input() value: any;
  formFromClass: FormGroup
  dataTable: any;

  constructor() { }

  ngOnInit(): void {
  }

}
