import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LumDateComponent } from './lum-date.component';

describe('LumDateComponent', () => {
  let component: LumDateComponent;
  let fixture: ComponentFixture<LumDateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LumDateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LumDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
