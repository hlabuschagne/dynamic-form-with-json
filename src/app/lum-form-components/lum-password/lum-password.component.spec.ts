import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LumPasswordComponent } from './lum-password.component';

describe('LumPasswordComponent', () => {
  let component: LumPasswordComponent;
  let fixture: ComponentFixture<LumPasswordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LumPasswordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LumPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
