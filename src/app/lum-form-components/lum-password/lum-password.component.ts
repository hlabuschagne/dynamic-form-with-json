import { FormGroup } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-lum-password',
  templateUrl: './lum-password.component.html',
  styleUrls: ['./lum-password.component.css']
})
export class LumPasswordComponent implements OnInit {
  label: string;
  placeHolder: string
  id: string;
  languageId: string;
  @Input() value: any;
  formFromClass: FormGroup;
  dataTable: any;

  constructor() { }

  ngOnInit(): void {
  }

}
