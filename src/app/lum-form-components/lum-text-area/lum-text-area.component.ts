import { FormGroup } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-lum-text-area',
  templateUrl: './lum-text-area.component.html',
  styleUrls: ['./lum-text-area.component.css']
})
export class LumTextAreaComponent implements OnInit {
  label: string;
  placeHolder: string;
  readOnlyProp: boolean;
  id: string;
  languageId: string;
  @Input() value: any;
  formFromClass: FormGroup
  dataTable: any;

  constructor() { }

  ngOnInit(): void {
  }

}
