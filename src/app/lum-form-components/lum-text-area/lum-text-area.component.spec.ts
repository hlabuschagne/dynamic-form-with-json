import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LumTextAreaComponent } from './lum-text-area.component';

describe('LumTextAreaComponent', () => {
  let component: LumTextAreaComponent;
  let fixture: ComponentFixture<LumTextAreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LumTextAreaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LumTextAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
