import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LumFormComponent } from './lum-form.component';

describe('LumFormComponent', () => {
  let component: LumFormComponent;
  let fixture: ComponentFixture<LumFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LumFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LumFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
