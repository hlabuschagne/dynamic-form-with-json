import { LumHiddenComponent } from './../lum-hidden/lum-hidden.component';
import { LumSelectComponent } from './../lum-select/lum-select.component';
import { LumDateComponent } from './../lum-date/lum-date.component';
import { LumTimeComponent } from './../lum-time/lum-time.component';
import { LumYesNoComponent } from './../lum-yes-no/lum-yes-no.component';
import { LumDropdownYesNoComponent } from './../lum-dropdown-yes-no/lum-dropdown-yes-no.component';
import { LumEmailComponent } from './../lum-email/lum-email.component';
import { LumPasswordComponent } from './../lum-password/lum-password.component';
import { LumTextAreaComponent } from './../lum-text-area/lum-text-area.component';
import { LumTextBoxComponent } from './../lum-text-box/lum-text-box.component';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, ComponentFactoryResolver, OnInit, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'app-lum-form',
  templateUrl: './lum-form.component.html',
  styleUrls: ['./lum-form.component.css']
})
export class LumFormComponent {
  form: FormGroup
  generateTextBox(id, label, form, fb: FormBuilder, container: ViewContainerRef, componentFactoryResolver: ComponentFactoryResolver) {
    //Text Box
    const lumTextBoxComponentFactory = componentFactoryResolver.resolveComponentFactory(LumTextBoxComponent);
    const componentRefTextBox = container.createComponent(
      lumTextBoxComponentFactory
    );
    componentRefTextBox.instance.id = id;
    componentRefTextBox.instance.label = label;
    componentRefTextBox.instance.placeHolder = label;
    componentRefTextBox.instance.formFromClass = form;
    //Add control to form
    form.addControl(
      componentRefTextBox.instance.id,

      fb.control('')
    );
  }
  generateTextArea(id, label, form, fb: FormBuilder, container: ViewContainerRef, componentFactoryResolver: ComponentFactoryResolver) {
    //Text Area
    const lumTextAreaComponentFactory = componentFactoryResolver.resolveComponentFactory(LumTextAreaComponent);
    const componentRefTextArea = container.createComponent(
      lumTextAreaComponentFactory
    );
    componentRefTextArea.instance.id = id;
    componentRefTextArea.instance.label = label;
    componentRefTextArea.instance.placeHolder = 'Provide a description if needed';
    componentRefTextArea.instance.formFromClass = form;
    //Add control to form
    form.addControl(
      componentRefTextArea.instance.id,

      fb.control(componentRefTextArea.instance.value || '')
    );
  }
  generateSelect(id, label, form, fb: FormBuilder, container: ViewContainerRef, componentFactoryResolver: ComponentFactoryResolver, selectOptions: any[]) {
    //Select
    const lumSelectComponentFactory = componentFactoryResolver.resolveComponentFactory(LumSelectComponent);
    const componentRefSelect = container.createComponent(
      lumSelectComponentFactory
    );
    componentRefSelect.instance.id = id;
    componentRefSelect.instance.label = label;
    componentRefSelect.instance.selectOptionsArray = selectOptions
    //Add control to form
    componentRefSelect.instance.formFromClass = form;
    form.addControl(
      componentRefSelect.instance.id,

      fb.control(componentRefSelect.instance.value || '')
    );
  }
  generateDate(id, label, form, fb: FormBuilder, container: ViewContainerRef, componentFactoryResolver: ComponentFactoryResolver) {
    // Date
    const lumDateComponentFactory = componentFactoryResolver.resolveComponentFactory(LumDateComponent);
    const componentRefDate = container.createComponent(
      lumDateComponentFactory
    );
    componentRefDate.instance.id = id;
    componentRefDate.instance.label = label;
    componentRefDate.instance.formFromClass = form;
    //Add control to form
    form.addControl(
      componentRefDate.instance.id,

      fb.control(componentRefDate.instance.value || '')
    );
  }
  generateTime(id, label, form, fb: FormBuilder, container: ViewContainerRef, componentFactoryResolver: ComponentFactoryResolver) {
    //Time
    const lumTimeComponentFactory = componentFactoryResolver.resolveComponentFactory(LumTimeComponent);
    const componentRefTime = container.createComponent(
      lumTimeComponentFactory
    );
    componentRefTime.instance.id = id;
    componentRefTime.instance.label = label;
    componentRefTime.instance.formFromClass = form;
    //Add control to form
    form.addControl(
      componentRefTime.instance.id,

      fb.control(componentRefTime.instance.value || '')
    );
  }
  generateYesNo(id, label, form, fb: FormBuilder, container: ViewContainerRef, componentFactoryResolver: ComponentFactoryResolver, language) {
    //Yes No
    const lumYesNoComponentFactory = componentFactoryResolver.resolveComponentFactory(LumYesNoComponent);
    const componentRefYesNo = container.createComponent(
      lumYesNoComponentFactory
    );
    componentRefYesNo.instance.id = id;
    componentRefYesNo.instance.label = label;
    componentRefYesNo.instance.language = language;
    componentRefYesNo.instance.formFromClass = form;
    //Add control to form
    form.addControl(
      componentRefYesNo.instance.id,

      fb.control(componentRefYesNo.instance.value)
    );
  }
  generateYesNoDropDown(id, label, form, fb: FormBuilder, container: ViewContainerRef, componentFactoryResolver: ComponentFactoryResolver, language) {
    //Yes No DropDown
    const lumYesNoDropDownComponentFactory = componentFactoryResolver.resolveComponentFactory(LumDropdownYesNoComponent);
    const componentRefYesNoDropDown = container.createComponent(
      lumYesNoDropDownComponentFactory
    );
    componentRefYesNoDropDown.instance.id = id;
    componentRefYesNoDropDown.instance.label = label;
    componentRefYesNoDropDown.instance.language = language;
    componentRefYesNoDropDown.instance.formFromClass = form;
    //Add control to form
    form.addControl(
      componentRefYesNoDropDown.instance.id,

      fb.control(componentRefYesNoDropDown.instance.value)
    );
  }
  generateEmail(id, label, form, fb: FormBuilder, container: ViewContainerRef, componentFactoryResolver: ComponentFactoryResolver) {
    //Email
    const lumEmailComponentFactory = componentFactoryResolver.resolveComponentFactory(LumEmailComponent);
    const componentRefEmail = container.createComponent(
      lumEmailComponentFactory
    );
    componentRefEmail.instance.id = id;
    componentRefEmail.instance.label = label;
    componentRefEmail.instance.placeHolder = 'example@provider.com';
    componentRefEmail.instance.formFromClass = form;
    componentRefEmail.instance.value;
    //Add control to form
    form.addControl(
      componentRefEmail.instance.id,

      fb.control(componentRefEmail.instance.value || '')
    );
  }
  generatePassword(id, label, form, fb: FormBuilder, container: ViewContainerRef, componentFactoryResolver: ComponentFactoryResolver) {
    //Password
    const lumPasswordComponentFactory = componentFactoryResolver.resolveComponentFactory(LumPasswordComponent);
    const componentRefPassword = container.createComponent(
      lumPasswordComponentFactory
    );
    componentRefPassword.instance.id = id;
    componentRefPassword.instance.label = label;
    componentRefPassword.instance.placeHolder = label;
    componentRefPassword.instance.formFromClass = form;
    //Add control to form
    form.addControl(
      componentRefPassword.instance.id,

      fb.control(componentRefPassword.instance.value || '')
    );
  }
  generateHidden(id, value, form, fb: FormBuilder, container: ViewContainerRef, componentFactoryResolver: ComponentFactoryResolver) {
    //Hidden
    const lumHiddenComponentFactory = componentFactoryResolver.resolveComponentFactory(LumHiddenComponent);
    const componentHidden = container.createComponent(
      lumHiddenComponentFactory
    );
    componentHidden.instance.id = id;
    componentHidden.instance.value = value;
    componentHidden.instance.formFromClass = form;
    //Add control to form
    form.addControl(
      componentHidden.instance.id,

      fb.control(componentHidden.instance.value)
    );
  }
}
