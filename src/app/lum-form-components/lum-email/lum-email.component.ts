import { FormGroup } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-lum-email',
  templateUrl: './lum-email.component.html',
  styleUrls: ['./lum-email.component.css']
})
export class LumEmailComponent implements OnInit {
  label: any;
  placeHolder: string;
  id: string;
  languageId: string;
  @Input() value: any;
  formFromClass: FormGroup
  dataTable: any;

  constructor() { }

  ngOnInit(): void {
  }

}
