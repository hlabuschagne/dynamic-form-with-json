import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LumEmailComponent } from './lum-email.component';

describe('LumEmailComponent', () => {
  let component: LumEmailComponent;
  let fixture: ComponentFixture<LumEmailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LumEmailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LumEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
