import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LumDropdownYesNoComponent } from './lum-dropdown-yes-no.component';

describe('LumDropdownYesNoComponent', () => {
  let component: LumDropdownYesNoComponent;
  let fixture: ComponentFixture<LumDropdownYesNoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LumDropdownYesNoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LumDropdownYesNoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
