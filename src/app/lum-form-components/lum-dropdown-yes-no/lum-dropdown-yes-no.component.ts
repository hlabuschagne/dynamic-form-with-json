import { FormGroup } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-lum-dropdown-yes-no',
  templateUrl: './lum-dropdown-yes-no.component.html',
  styleUrls: ['./lum-dropdown-yes-no.component.css']
})
export class LumDropdownYesNoComponent implements OnInit {
  label: string;
  id: string;
  language: string;
  @Input() value: any;
  formFromClass: FormGroup
  dataTable: any;

  constructor() { }

  ngOnInit(): void {
  }

}
