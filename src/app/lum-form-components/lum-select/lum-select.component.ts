import { FormGroup } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-lum-select',
  templateUrl: './lum-select.component.html',
  styleUrls: ['./lum-select.component.css']
})
export class LumSelectComponent implements OnInit {
  label: string;
  selectOptionsArray: any[];
  id: string;
  languageId: string;
  @Input() value: any;
  formFromClass: FormGroup
  dataTable: any;

  constructor() { }

  ngOnInit(): void {
  }

}
