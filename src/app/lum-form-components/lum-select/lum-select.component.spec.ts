import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LumSelectComponent } from './lum-select.component';

describe('LumSelectComponent', () => {
  let component: LumSelectComponent;
  let fixture: ComponentFixture<LumSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LumSelectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LumSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
