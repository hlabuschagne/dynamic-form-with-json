import { FormGroup } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-lum-yes-no',
  templateUrl: './lum-yes-no.component.html',
  styleUrls: ['./lum-yes-no.component.css']
})
export class LumYesNoComponent implements OnInit {
  label: string;
  id: string;
  language: string;
  @Input() value: any;
  formFromClass:FormGroup
  dataTable: any;

  constructor() { }

  ngOnInit(): void {

  }

}
