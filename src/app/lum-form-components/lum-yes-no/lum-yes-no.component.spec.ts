import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LumYesNoComponent } from './lum-yes-no.component';

describe('LumYesNoComponent', () => {
  let component: LumYesNoComponent;
  let fixture: ComponentFixture<LumYesNoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LumYesNoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LumYesNoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
