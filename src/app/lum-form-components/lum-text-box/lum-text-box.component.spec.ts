import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LumTextBoxComponent } from './lum-text-box.component';

describe('LumTextBoxComponent', () => {
  let component: LumTextBoxComponent;
  let fixture: ComponentFixture<LumTextBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LumTextBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LumTextBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
