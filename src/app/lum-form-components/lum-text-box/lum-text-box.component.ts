import { FormGroup } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-lum-text-box',
  templateUrl: './lum-text-box.component.html',
  styleUrls: ['./lum-text-box.component.css']
})
export class LumTextBoxComponent implements OnInit {
  label: string;
  placeHolder: string;
  id: string;
  languageId: string;
  @Input() value: any;
  formFromClass: FormGroup;
  dataTable: any;

  constructor() { }

  ngOnInit(): void {
  }

}
