import { RxReactiveDynamicFormsModule } from '@rxweb/reactive-dynamic-forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LumDateComponent } from './lum-form-components/lum-date/lum-date.component';
import { LumSelectComponent } from './lum-form-components/lum-select/lum-select.component';
import { LumTextAreaComponent } from './lum-form-components/lum-text-area/lum-text-area.component';
import { LumTextBoxComponent } from './lum-form-components/lum-text-box/lum-text-box.component';
import { LumTimeComponent } from './lum-form-components/lum-time/lum-time.component';
import { LumYesNoComponent } from './lum-form-components/lum-yes-no/lum-yes-no.component';
import { LumEmailComponent } from './lum-form-components/lum-email/lum-email.component';
import { LumPasswordComponent } from './lum-form-components/lum-password/lum-password.component';
import { LumDropdownYesNoComponent } from './lum-form-components/lum-dropdown-yes-no/lum-dropdown-yes-no.component';
import { LumHiddenComponent } from './lum-form-components/lum-hidden/lum-hidden.component';
import { HttpClientModule } from '@angular/common/http';
import { LumFormComponent } from './lum-form-components/lum-form/lum-form.component';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';

@NgModule({
  declarations: [
    AppComponent,
    LumDateComponent,
    LumSelectComponent,
    LumTextAreaComponent,
    LumTextBoxComponent,
    LumTimeComponent,
    LumYesNoComponent,
    LumEmailComponent,
    LumPasswordComponent,
    LumDropdownYesNoComponent,
    LumHiddenComponent,
    LumFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    RxReactiveDynamicFormsModule,
    RxReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
