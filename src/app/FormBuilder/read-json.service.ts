import { QuestionBase } from './QuestionBase';
import { map } from 'rxjs/operators';
import { FormGeneratorService } from './form-generator.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ReadJsonService {
  languageSelectedUser: string;
  questionsVar: any;
  languageLabelSelected: string[];
  languageLabelVar: any;
  constructor(private httpClient: HttpClient, private formCreator: FormGeneratorService,){}

  getJsonInputFields(language, fb, form, container, componentFactoryResolver, jsonFile) {
		this.httpClient.get(jsonFile)
			.pipe(map((fields: any[]) => {
				return fields.map(field => {
					return new QuestionBase<any>(field)
				})
			}))
			.subscribe((fields: any) => {
				this.questionsVar = fields;
        // this.qcs.createForm(this.questionsVar, this.newBlankForm)
				this.questionsVar.forEach((item) => {
          for (let index = 0; index < item.newLabel.length; index++) {
						const element = item.newLabel[index];
						const key = Object.keys(element).toString();
						this.languageLabelSelected = Object.keys(element);
						if (language === key) {
							this.languageLabelVar = element[language];
							if (
								element.LanguageLabelSelected == undefined ||
								element.LanguageLabelSelected == null
							) {
                this.formCreator.addComponentToForm(item.id, item.type, this.languageLabelVar, form,  fb, container, componentFactoryResolver, item.value, item.options, language)
              }
            }
          }

				});
			});
}
}
