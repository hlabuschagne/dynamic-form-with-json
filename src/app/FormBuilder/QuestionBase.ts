export class QuestionBase<T> {
  value: T|undefined;
  id: string;
  name: string;
  DBTable: string;
  newLabel: [
    {Afrikaans: string},
    {English: string},
    {LanguageLabelSelected: any}
  ][]
  DBField: string;
  required: boolean;
  type: string;
  options: {key: string, value: string}[];
  validator: string;

  constructor(options: {
      value?: T;
      inputComponent?: any;
      id?: string;
      validator?: string;
      name?: string;
      label?: string;
      newLabel?: [
        {Afrikaans: string},
        {English: string},
        {LanguageLabelSelected: any[]}
      ][];
      DBTable?: string;
      DBField?: string;
      required?: boolean;
      type?: string;
      options?: {key: string, value: string}[];
    } = {}) {
    this.value = options.value;
    this.validator = options.validator || '';
    this.name = options.name || '';
    this.id = options.id || '';
    this.newLabel = options.newLabel || [];
    this.DBTable = options.DBTable || '';
    this.DBField = options.DBField || '';
    this.required = !!options.required;
    this.type = options.type || '';
    this.options = options.options || [];
  }
}
