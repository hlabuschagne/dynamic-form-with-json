import { LumFormComponent } from './../lum-form-components/lum-form/lum-form.component';
import { Injectable, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import {
  FormBuilder,
} from '@angular/forms';

@Injectable()
export class FormGeneratorService {
  FormComponent: any = new LumFormComponent();
  constructor() { }
  addComponentToForm(id, type, label, form, fb: FormBuilder, container: ViewContainerRef, componentFactoryResolver: ComponentFactoryResolver, value?: any, selectOptions?: any[], languageSelected?: string) {
    if (type == "date") {
      this.FormComponent.generateDate(id, label, form, fb, container, componentFactoryResolver)
    }
    if (type == "select") {
      //Select
      this.FormComponent.generateSelect(id, label, form, fb, container, componentFactoryResolver, selectOptions)
    }
    if (type == "textarea") {
      //Text Area
      this.FormComponent.generateTextArea(id, label, form, fb, container, componentFactoryResolver)
    }
    if (type == "textbox") {
      //Text Box
      this.FormComponent.generateTextBox(id, label, form, fb, container, componentFactoryResolver)
    }
    if (type == "password") {
      //Password
      this.FormComponent.generatePassword(id, label, form, fb, container, componentFactoryResolver)
    }
    if (type == "email") {
      //Email
      this.FormComponent.generateEmail(id, label, form, fb, container, componentFactoryResolver)
    }
    if (type == "time") {
      //Time
      this.FormComponent.generateTime(id, label, form, fb, container, componentFactoryResolver)
    }
    if (type == "yesno") {
      //Yes No
      this.FormComponent.generateYesNo(id, label, form, fb, container, componentFactoryResolver, languageSelected)
    }
    if (type == "yesnodropdown") {
      //Yes No DropDown
      this.FormComponent.generateYesNoDropDown(id, label, form, fb, container, componentFactoryResolver, languageSelected)
    }
    if (type == "hidden") {
      //Hidden
      this.FormComponent.generateHidden(id, value, form, fb, container, componentFactoryResolver)
    }
  }
}